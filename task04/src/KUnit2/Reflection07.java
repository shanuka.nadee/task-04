package KUnit2;

import java.lang.reflect.Field;


public class Reflection07 {
	public static void main(String[] args) throws Exception {
		SimpleMainProcess sh = new SimpleMainProcess();
		Field[] flds = sh.getClass().getDeclaredFields();
		System.out.printf("There are %d fields\n", flds.length);

		for (Field fld : flds) {
			fld.setAccessible(true);
			System.out.printf("field name = %s type = %s value = %d\n", 
					fld.getName(),
					fld.getType(), 
					fld.getLong(sh));
		}
	}
}
