package KUnit2;

import java.lang.reflect.Field;

public class Reflection08 {

	public static void main(String[] args) throws Exception {
		SimpleMainProcess sh = new SimpleMainProcess();
		Field[] fields = sh.getClass().getDeclaredFields();
		System.out.printf("There are %d fields\n", fields.length);
		for (Field fld : fields) {
			fld.setAccessible(true);
			long x = fld.getLong(sh);
			x++;
			fld.setLong(sh, x);
			System.out.printf("field name = %s type = %s value = %d\n", 
					fld.getName(), 
					fld.getType(), 
					fld.getLong(sh));
		}
	}
}
