package KUnit2;

import java.lang.reflect.Field;


public class Reflection04 {
	public static void main(String[] args) throws Exception {
		SimpleMainProcess sh = new SimpleMainProcess();
		java.lang.reflect.Field[] flds = sh.getClass().getFields();
		System.out.printf("There are %d fields\n", flds.length);
		for (java.lang.reflect.Field fld : flds) {
			System.out.printf("field name = %s type = %s value = %d\n",
					fld.getName(),
					fld.getType(),
					fld.getLong(sh)
					);
		}
	}
}

