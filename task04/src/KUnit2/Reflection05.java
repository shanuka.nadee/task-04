package KUnit2;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class Reflection05 {
	public static void main(String[] args) throws Exception {
		SimpleMainProcess sh = new SimpleMainProcess();
		Field[] flds = sh.getClass().getDeclaredFields();
		System.out.printf("There are %d fields\n", flds.length);

		for (Field f : flds) {
			System.out.printf("field name = %s type = %s value = %d\n", 
					f.getName(),
					f.getType(),
					f.getLong(sh));
		}
	}
}

