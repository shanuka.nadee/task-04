package KUnit2;

import java.lang.reflect.Field;

public class Reflection06 {

	public static void main(String[] args) throws Exception {
		SimpleMainProcess sh = new SimpleMainProcess();
		Field[] fields = sh.getClass().getDeclaredFields();
		System.out.printf("There are %d fields\n", fields.length);

		for (Field fld : fields) {
			System.out.printf("field name = %s type = %s accessible = %s\n",
					fld.getName(), fld.getType(),
					fld.isAccessible());
		}
	}
}
