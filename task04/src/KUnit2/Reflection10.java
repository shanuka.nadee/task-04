package KUnit2;

import java.lang.reflect.Method;

public class Reflection10 {
	public static void main(String[] args) throws Exception {
		SimpleMainProcess sh = new SimpleMainProcess();
		Method mthd = sh.getClass().getDeclaredMethod("setX", long.class);
		mthd.setAccessible(true);
		mthd.invoke(sh, 86);
		System.out.println(sh);
	}
}
