package KUnit2;

import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class Reflection09 {
	public static void main(String[] args) throws Exception {
		SimpleMainProcess sh = new SimpleMainProcess();
		Method[] mthd = sh.getClass().getMethods();
		System.out.printf("There are %d methods\n", mthd.length);

		for (Method md : mthd) {
			System.out.printf("method name = %s type = %s parameters = ", 
					md.getName(),
					md.getReturnType());
			Class[] types = md.getParameterTypes();
			for (Class cls : types) {
				System.out.print(cls.getName() + " ");
			}
			System.out.println();
		}
	}
}
